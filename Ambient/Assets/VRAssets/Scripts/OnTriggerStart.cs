﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnTriggerStart : MonoBehaviour {

    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "player")
        {
            SceneManager.LoadScene("main/iuhygfmnb");
        }
        
    }
}
