﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keyItem : MonoBehaviour
{
    public static int keyCount = 0;
    public string interactText = "Press H To Interact";
    public bool hasEntered = false;
    public GameObject key;


    void OnTriggerEnter(Collider collider)
    {
        hasEntered = true;
        if ( collider.gameObject == key)
        {
            keyCount++;
            Debug.Log("Picking up item");
            Destroy(gameObject);
        }
    }

}

