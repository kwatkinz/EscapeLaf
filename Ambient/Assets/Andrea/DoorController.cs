﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{

    public bool isOpen;
    public Vector3 openAngle = new Vector3(0, 0, 90);
    public Vector3 closeAngle = new Vector3(0, 0, 0);
    public GameObject door;
    public Collider doorCollider;
    public float speed = 2f;
    private AudioSource source;
    private Animation doorAnim;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        doorAnim = GetComponent<Animation>();
        // door = GetComponent<GameObject>();
        doorCollider = GetComponent<Collider>();
        if (!source)
        {
            Debug.LogWarning(gameObject.name + " doesn't have an AudioSource");
        }
    }

    void Update()
    {
        door = GetComponent<GameObject>();
        
        if (isOpen)
            {
                doorCollider.transform.localRotation = Quaternion.Slerp(doorCollider.transform.localRotation, Quaternion.Euler(openAngle), Time.deltaTime * speed);
            }
            else
            {
                doorCollider.transform.localRotation = Quaternion.Slerp(doorCollider.transform.localRotation, Quaternion.Euler(closeAngle), Time.deltaTime * speed);
            }
        
        if (Input.GetKey(KeyCode.Tab))
        {
            Toggle();
        }
    }

    public void Open()
    {
        if (!isOpen)
        {

            isOpen = true;
            source.Play();
        }
    }

    public void Close()
    {
        if (isOpen)
        {
            isOpen = false;
            source.Play();
        }
    }

    public void Toggle()
    {
        if (isOpen)
        {
            Close();
        }
        else
        {
            Open();
        }
    }
}