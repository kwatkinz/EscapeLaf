﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextNotification : MonoBehaviour {

    public AudioSource textMessage;
    private bool entertedAlready = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !entertedAlready)
        {
            textMessage.Play();
            entertedAlready = true;
        }
    }
}
