﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawerOpen : MonoBehaviour
{
    public Transform cabinet;
    public float SmoothRotation;
    public string interactText = "Press F To Interact";
    public GUIStyle InteractTextStyle;

    private bool init = false;
    public bool hasEntered = false;
    private bool doorOpen = false;
    private Rect interactTextRect;
    private AudioSource source;
    public GameObject Trigger;
    private Vector3 oringalTransform;


    void Start()
    {
        //cabinet = GetComponent<Transform>();
        source = GetComponent<AudioSource>();
        //Check if Door Game Object is properly assigned
        if (cabinet == null)
        {
            Debug.LogError(this + " :: Door Object Not Defined!");
        }

        Vector3 oringalTransform = new Vector3(cabinet.transform.localPosition.x, cabinet.transform.localPosition.y, cabinet.transform.localPosition.z);
        //Init Interact text Rect
        Vector2 textSize = InteractTextStyle.CalcSize(new GUIContent(interactText));
        interactTextRect = new Rect(Screen.width / 2 - textSize.x / 2, Screen.height - (textSize.y + 5), textSize.x, textSize.y);

        init = true;
    }

    void Update()
    {
        if (!init)
            return;
        HandleUserInputOpen();
        HandleUserInputClose();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player")
        {
            hasEntered = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        hasEntered = false;
    }

    void OnGUI()
    {
        if (!init || !hasEntered)
            return;

        GUI.Label(interactTextRect, interactText, InteractTextStyle);
    }

    void HandleUserInputOpen()
    {
        if (Input.GetKeyDown(KeyCode.F) && hasEntered)
        {
            Vector3 shift = new Vector3(cabinet.transform.localPosition.x - 0.1f, oringalTransform.y, oringalTransform.z);
            source.Play();
            cabinet.Translate(shift);
        }
    }

    void HandleUserInputClose()
    {
        if (Input.GetKeyDown(KeyCode.G) && hasEntered)
        {
            Vector3 shift = new Vector3(cabinet.transform.localPosition.x + 0.1f, oringalTransform.y, oringalTransform.z);
            source.Play();
            cabinet.Translate(shift);
        }
    }
}
